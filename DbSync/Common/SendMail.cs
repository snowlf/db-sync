﻿using System.Net.Mail;

namespace DbSync
{
    public class SendMail
    {

        public static bool Send(string from, string to, string subject, bool isBodyHtml, string body, string smtpHost, string userName, string password)
        {
            MailMessage mm = new MailMessage();
            mm.From = new MailAddress(from);
            string[] ts = to.Split(',');
            bool isSuccess = true;
            foreach (string t in ts)
            {
                mm.To.Add(new MailAddress(t.Trim()));
            }
            mm.Subject = subject;
            mm.IsBodyHtml = isBodyHtml;
            mm.Body = body;
            try
            {
                SmtpClient sc = new SmtpClient();
                sc.Host = smtpHost;
                //sc.Port = port;
                sc.UseDefaultCredentials = true;//winform中不受影响，asp.net中，false表示不发送身份严正信息 
                //sc.EnableSsl = true;//如果服务器不支持ssl则报，服务器不支持安全连接 错误 
                sc.Credentials = new System.Net.NetworkCredential(userName, password);
                sc.DeliveryMethod = SmtpDeliveryMethod.Network;
                sc.Send(mm);
            }
            catch
            {
                 
                isSuccess = false;
            }
            return isSuccess;
        }
    }
}
