﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;

namespace DbSync
{
    /// <summary>
    /// Json操作帮助类
    /// </summary>
    public class JsonHelper : DynamicObject
    {
        #region json和对象之间的转换
      
        /// <summary>
        /// 对象转换成json
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="jsonObject">需要格式化的对象</param>
        /// <returns>Json格式字符串</returns>
        public static string ObjToJson<T>(T jsonObject)
        {
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(T));
            string json = null;
            using (MemoryStream ms = new MemoryStream()) //定义一个stream用来存放序列化之后的内容
            {
                serializer.WriteObject(ms, jsonObject);
                json = Encoding.UTF8.GetString(ms.ToArray()); //将stream读取成一个字符串形式的数据，并且返回
                ms.Close();
            }

            return DateHt(json);
        }

        /// <summary>
        /// json字符串转换成对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="json">要转换成对象的json字符串</param>
        /// <returns></returns>
        public static T JsonToObj<T>(string json)
        {
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(T));
            T obj = default(T);
            try
            {
                using (MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(json)))
                {
                    obj = (T)serializer.ReadObject(ms);
                    ms.Close();
                }
            }
            catch (Exception ex)
            {
            }
            return obj;
        }

        #endregion

        #region 日期转换为本地时间
        /// <summary>
        /// json日期转换为本地时间
        /// </summary>
        /// <param name="jsonText"></param>
        /// <returns></returns>
        public static string DateHt(string jsonText)
        {
            jsonText = Regex.Replace(jsonText, @"\\/Date\(\d{13}\+\d{4}\)\\/", match =>
            {
                DateTime dt = new DateTime(1970, 1, 1);
                dt = dt.AddMilliseconds(long.Parse(match.Groups[0].Value.Substring(7, 13)));
                dt = dt.ToLocalTime();
                return dt.ToString("yyyy-MM-dd");//设定时间格式
            });
            return jsonText;
        }
        #endregion

        #region Json动态解析方法
        /// <summary>
        /// 把Json字符串转为Json对象
        /// </summary>
        /// <param name="json">String类型 符合Json规范的字符串</param>
        /// <returns>Json对象或Json数组List</returns>
        public static dynamic ToObject(string json)
        {
            return ToObject(json, Encoding.Unicode);
        }
        /// <summary>
        /// 把Json字符串转为Json对象（可自定义编码）
        /// </summary>
        /// <param name="json">String类型 符合Json规范的字符串</param>
        ///  <param name="encoding">自定义编码</param>
        /// <returns>Json对象或Json数组List</returns>
        public static dynamic ToObject(string json, Encoding encoding)
        {
            try
            {
                using (var reader = JsonReaderWriterFactory.CreateJsonReader(encoding.GetBytes(json), XmlDictionaryReaderQuotas.Max))
                {
                    return ToValue(XElement.Load(reader));
                }
            }
            catch (Exception exc) { return exc.Message; }
        }
        /// <summary>
        /// 把Stream类型的Json数据转为Json对象
        /// </summary>
        /// <param name="stream">Stream类型Json数据</param>
        /// <returns>Json对象或Json数组List</returns>
        public static dynamic ToObject(Stream stream)
        {
            try
            {
                using (var reader = JsonReaderWriterFactory.CreateJsonReader(stream, XmlDictionaryReaderQuotas.Max))
                {
                    return ToValue(XElement.Load(reader));
                }
            }
            catch (Exception exc) { return exc.Message; }
        }
        /// <summary>
        /// 把Stream类型的Json数据转为Json对象
        /// </summary>
        /// <param name="stream">Stream类型Json数据</param>
        /// <param name="encoding">自定义编码</param>
        /// <returns>Json对象或Json数组List</returns>
        public static dynamic ToObject(Stream stream, Encoding encoding)
        {
            try
            {
                using (var reader = JsonReaderWriterFactory.CreateJsonReader(stream, encoding, XmlDictionaryReaderQuotas.Max, _ => { }))
                {
                    return ToValue(XElement.Load(reader));
                }
            }
            catch (Exception exc) { return exc.Message; }
        }
        /// <summary>
        /// 把对象或者List数组转为Json字符串（支持复杂对象转换）
        /// </summary>
        /// <param name="obj">object</param>
        /// <returns>Json字符串</returns>
        public static string ToJson(object obj)
        {
            return obj == null ? "" : CreateJsonString(new XStreamingElement("root", CreateTypeAttr(GetJsonType(obj)), CreateJsonNode(obj)));
        }
        #endregion

        #region 私有方法
        /// <summary>
        /// 枚举定义Json类型
        /// </summary>
        private enum JsonType
        {
            @string, number, boolean, @object, array, @null
        }
        /// <summary>
        /// 处理不同类型的Value
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        private static dynamic ToValue(XElement element)
        {
            var type = (JsonType)Enum.Parse(typeof(JsonType), element.Attribute("type").Value);
            switch (type)
            {
                case JsonType.boolean:
                    return (bool)element;
                case JsonType.number:
                    return (double)element;
                case JsonType.@string:
                    return (string)element;
                case JsonType.@object:
                case JsonType.array:
                    return new JsonHelper(element, type);
                case JsonType.@null:
                default:
                    return null;
            }
        }
        /// <summary>
        /// 获取Json字段类型
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private static JsonType GetJsonType(object obj)
        {
            if (obj == null) return JsonType.@null;

            switch (Type.GetTypeCode(obj.GetType()))
            {
                case TypeCode.Boolean:
                    return JsonType.boolean;
                case TypeCode.String:
                case TypeCode.Char:
                case TypeCode.DateTime:
                    return JsonType.@string;
                case TypeCode.Int16:
                case TypeCode.Int32:
                case TypeCode.Int64:
                case TypeCode.UInt16:
                case TypeCode.UInt32:
                case TypeCode.UInt64:
                case TypeCode.Single:
                case TypeCode.Double:
                case TypeCode.Decimal:
                case TypeCode.SByte:
                case TypeCode.Byte:
                    return JsonType.number;
                case TypeCode.Object:
                    return (obj is IEnumerable) ? JsonType.array : JsonType.@object;
                case TypeCode.DBNull:
                case TypeCode.Empty:
                default:
                    return JsonType.@null;
            }
        }

        private static XAttribute CreateTypeAttr(JsonType type)
        {
            return new XAttribute("type", type.ToString());
        }

        private static object CreateJsonNode(object obj)
        {
            var type = GetJsonType(obj);
            switch (type)
            {
                case JsonType.@string:
                case JsonType.number:
                    return obj;
                case JsonType.boolean:
                    return obj.ToString().ToLower();
                case JsonType.@object:
                    return CreateXObject(obj);
                case JsonType.array:
                    return CreateXArray(obj as IEnumerable);
                case JsonType.@null:
                default:
                    return null;
            }
        }
        private static IEnumerable<XStreamingElement> CreateXArray<T>(T obj) where T : IEnumerable
        {
            return obj.Cast<object>()
                .Select(o => new XStreamingElement("item", CreateTypeAttr(GetJsonType(o)), CreateJsonNode(o)));
        }
        private static IEnumerable<XStreamingElement> CreateXObject(object obj)
        {
            return obj.GetType()
                .GetProperties(BindingFlags.Public | BindingFlags.Instance)
                .Select(pi => new { Name = pi.Name, Value = pi.GetValue(obj, null) })
                .Select(a => new XStreamingElement(a.Name, CreateTypeAttr(GetJsonType(a.Value)), CreateJsonNode(a.Value)));
        }
        private static string CreateJsonString(XStreamingElement element)
        {
            try
            {
                using (var ms = new MemoryStream())
                using (var writer = JsonReaderWriterFactory.CreateJsonWriter(ms, Encoding.Unicode))
                {
                    element.WriteTo(writer);
                    writer.Flush();
                    return Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            catch (Exception exc) { return exc.Message; }
        }
        //动态处理对象和数组List
        readonly XElement xml;
        readonly JsonType jsonType;
        /// <summary>
        /// 创建无参构造函数
        /// </summary>
        public JsonHelper()
        {
            xml = new XElement("root", CreateTypeAttr(JsonType.@object));
            jsonType = JsonType.@object;
        }
        private JsonHelper(XElement element, JsonType type)
        {
            Debug.Assert(type == JsonType.array || type == JsonType.@object);
            xml = element;
            jsonType = type;
        }
        public bool IsObject { get { return jsonType == JsonType.@object; } }
        public bool IsArray { get { return jsonType == JsonType.array; } }
        /// <summary>
        /// 类型属性验证
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool IsDefined(string name)
        {
            return IsObject && (xml.Element(name) != null);
        }
        /// <summary>
        /// 类型属性验证
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public bool IsDefined(int index)
        {
            return IsArray && (xml.Elements().ElementAtOrDefault(index) != null);
        }

        /// <summary>
        /// 删除属性
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool Delete(string name)
        {
            var elem = xml.Element(name);
            if (elem != null)
            {
                elem.Remove();
                return true;
            }
            else return false;
        }

        /// <summary>
        /// 删除属性
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public bool Delete(int index)
        {
            var elem = xml.Elements().ElementAtOrDefault(index);
            if (elem != null)
            {
                elem.Remove();
                return true;
            }
            else return false;
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="binder"></param>
        /// <param name="args"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public override bool TryInvoke(InvokeBinder binder, object[] args, out object result)
        {
            result = (IsArray)
                ? Delete((int)args[0])
                : Delete((string)args[0]);
            return true;
        }
        /// <summary>
        /// 重写TryInvokeMember方法
        /// </summary>
        /// <param name="binder"></param>
        /// <param name="args"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public override bool TryInvokeMember(InvokeMemberBinder binder, object[] args, out object result)
        {
            if (args.Length > 0)
            {
                result = null;
                return false;
            }
            result = IsDefined(binder.Name);
            return true;
        }

        private bool TryGet(XElement element, out object result)
        {
            if (element == null)
            {
                result = null;
                return false;
            }

            result = ToValue(element);
            return true;
        }
        public override bool TryGetIndex(GetIndexBinder binder, object[] indexes, out object result)
        {
            return (IsArray)
                ? TryGet(xml.Elements().ElementAtOrDefault((int)indexes[0]), out result)
                : TryGet(xml.Element((string)indexes[0]), out result);
        }
        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            return (IsArray)
                ? TryGet(xml.Elements().ElementAtOrDefault(int.Parse(binder.Name)), out result)
                : TryGet(xml.Element(binder.Name), out result);
        }
        private bool TrySet(string name, object value)
        {
            var type = GetJsonType(value);
            var element = xml.Element(name);
            if (element == null)
            {
                xml.Add(new XElement(name, CreateTypeAttr(type), CreateJsonNode(value)));
            }
            else
            {
                element.Attribute("type").Value = type.ToString();
                element.ReplaceNodes(CreateJsonNode(value));
            }

            return true;
        }
        private bool TrySet(int index, object value)
        {
            var type = GetJsonType(value);
            var e = xml.Elements().ElementAtOrDefault(index);
            if (e == null)
            {
                xml.Add(new XElement("item", CreateTypeAttr(type), CreateJsonNode(value)));
            }
            else
            {
                e.Attribute("type").Value = type.ToString();
                e.ReplaceNodes(CreateJsonNode(value));
            }

            return true;
        }
        public override bool TrySetIndex(SetIndexBinder binder, object[] indexes, object value)
        {
            return (IsArray)
                ? TrySet((int)indexes[0], value)
                : TrySet((string)indexes[0], value);
        }
        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            return (IsArray)
                ? TrySet(int.Parse(binder.Name), value)
                : TrySet(binder.Name, value);
        }
        public override IEnumerable<string> GetDynamicMemberNames()
        {
            return (IsArray)
                ? xml.Elements().Select((x, i) => i.ToString())
                : xml.Elements().Select(x => x.Name.LocalName);
        }
        /// <summary>
        /// 重写ToString方法
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            //无法序列化就剔除
            foreach (var elem in xml.Descendants().Where(x => x.Attribute("type").Value == "null"))
            {
                elem.RemoveNodes();
            }
            return CreateJsonString(new XStreamingElement("root", CreateTypeAttr(jsonType), xml.Elements()));
        }
        #endregion
    }
}

