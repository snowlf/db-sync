﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using DbSync;
using DbSync.Common;

namespace DbSync
{
    class Program
    {
        static void Main(string[] args)
        {
            /////////////////////////////////////////////
            //   源数据库 [同步]到 目标数据库            //
            /////////////////////////////////////////////

          
            Console.Title = "数据同步";
            //禁用控制台窗口关闭按钮
            //DisableCloseButton(Console.Title);

            Console.WriteLine(DateTime.Now + " 开始执行...");
            DbSyncCore.StartSync();
            

             Console.WriteLine(DateTime.Now + "\r\n执行完毕,请按任意键退出...");

          
            Console.ReadLine();        
        }
       
    }
}
