﻿using System.Collections.Generic;

namespace DbSync
{

    public class AppConfig
    {
        public string Title { get; set; }
        public List<SyncItem> SyncItems { get; set; }

    }
    public class SyncItem
    {
        /// <summary>
        /// 任务标识
        /// </summary>
        public string Index { get; set; }
        /// <summary>
        /// 源数据库
        /// </summary>
        public string  Source { get; set; }
        /// <summary>
        /// 目标数据库
        /// </summary>
        public List<string> Target { get; set; }
        public bool UseStructureSync { get; set; } = true;//结构同步
        public bool UseDataSync { get; set; } = true; // 数据同步
        /// <summary>
        /// 排序字段,多个以逗号分隔
        /// </summary>
        public string OrderBy { get; set; }
        /// <summary>
        /// 同步方式 1 全量同步(会执行删除),2 增量同步
        /// </summary>
        public int SyncType { get; set; } = 1;
        /// <summary>
        /// 是否双向同步
        /// </summary>
        public bool IsBothway { get; set; } = false;
        public bool IsStartRest { get; set; } = true;
        /// <summary>
        /// 包含表
        /// </summary>
        public string IncludeTable { get; set; }
        /// <summary>
        /// 忽略表
        /// </summary>
        public string IgnoreTable { get; set; }
        /// <summary>
        /// 执行计划
        /// </summary>
        //public string ExecuteCroe { get; set; }
        public List<string> ExecuteCroe { get; set; }
    }
}
