﻿using DbSync.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbSync.Provider
{
    class MsSqlProxy : IDbProxy
    {
        
        public bool ConnectStatus => throw new NotImplementedException();

        public DBHelper DB => throw new NotImplementedException();

        public MsSqlProxy(string conn)
        {

        }
        public int AddTableColumn(string tableName, KeyValuePair<string, string> column)
        {
            throw new NotImplementedException();
        }

        public int AddTableIndex(string tableName, KeyValuePair<string, string> index)
        {
            throw new NotImplementedException();
        }

        public bool CreateTable(string createTableSql)
        {
            throw new NotImplementedException();
        }

        public int CurrentConnectedCount()
        {
            throw new NotImplementedException();
        }

        public int DeleteData(string table, string pkey, List<string> ids)
        {
            throw new NotImplementedException();
        }

        public int DeleteTableColumn(string tableName, KeyValuePair<string, string> column)
        {
            throw new NotImplementedException();
        }

        public int DeleteTableIndex(string tableName, KeyValuePair<string, string> index)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public int DropTable(string tableName)
        {
            throw new NotImplementedException();
        }

        public int Execute(string sql)
        {
            throw new NotImplementedException();
        }

        public string GetCreateTableInfo(string tableName)
        {
            throw new NotImplementedException();
        }

        public string getData(string sql, int index = 0)
        {
            throw new NotImplementedException();
        }

        public List<List<string>> getDatas(string sql)
        {
            throw new NotImplementedException();
        }

        public List<Dictionary<string, string>> getDatasDic(string sql, List<string> columnSelect = null, int columnSelectType = 1)
        {
            throw new NotImplementedException();
        }

        public Dictionary<string, string> GetIndexMap(string tableName)
        {
            throw new NotImplementedException();
        }

        public List<Dictionary<string, string>> GetTableData(string tableName)
        {
            throw new NotImplementedException();
        }

        public List<Dictionary<string, string>> GetTableDataByKeys(string tableName, string keyName, List<string> keys)
        {
            throw new NotImplementedException();
        }

        public List<Dictionary<string, string>> GetTableDataByPage(string tableName, int pageindex, int pagesize, string pkey, string extraField = "pkIndex", KeyValuePair<string, string> orderby = default)
        {
            throw new NotImplementedException();
        }

        public List<string> GetTables()
        {
            throw new NotImplementedException();
        }

        public Dictionary<string, string> GetTableStructure(string tableName)
        {
            throw new NotImplementedException();
        }

        public Dictionary<string, string> GetTableStructure(string tableName, out string key, out string auto_increment)
        {
            throw new NotImplementedException();
        }

        public int InsertData(string table, List<Dictionary<string, string>> datas)
        {
            throw new NotImplementedException();
        }

        public int LimitConnectedCount()
        {
            throw new NotImplementedException();
        }

        public int ModifyTableColumn(string tableName, KeyValuePair<string, string> column)
        {
            throw new NotImplementedException();
        }

        public int ModifyTableIndex(string tableName, KeyValuePair<string, string> index)
        {
            throw new NotImplementedException();
        }

        public int UpdateData(string table, List<Dictionary<string, string>> datas)
        {
            throw new NotImplementedException();
        }
    }
}
