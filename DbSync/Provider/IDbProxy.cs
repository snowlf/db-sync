﻿using DbSync.Common;
using System;
using System.Collections.Generic;

namespace DbSync.Provider
{
    /// <summary>
    /// 多数据库支持
    /// </summary>
    public interface IDbProxy: IDisposable
    {
        bool ConnectStatus { get; }
        DBHelper DB { get; }
        //show status like 'Threads%';  -- 当前连接数        
        //查询当前连接数
        int CurrentConnectedCount();
        //show variables like '%max_connections%';  -- 最大支持的连接数
        //查询最大连接数
        int LimitConnectedCount();
        //查询表List
        List<string> GetTables();
        //查询表结构,同时返回主键key
        Dictionary<string, string> GetTableStructure(string tableName);
        Dictionary<string, string> GetTableStructure(string tableName,out string key,out string auto_increment);
        //查询表信息
        string GetCreateTableInfo(string tableName);
        //新增表 修改表 删除表
        bool CreateTable(string createTableSql);
        int DropTable(string tableName);
        //修改表
        int AddTableColumn(string tableName, KeyValuePair<string,string> column);
        int ModifyTableColumn(string tableName, KeyValuePair<string, string> column);
        int DeleteTableColumn(string tableName, KeyValuePair<string, string> column);
        //查询索引
        Dictionary<string, string> GetIndexMap(string tableName);
        //修改索引
        int AddTableIndex(string tableName, KeyValuePair<string, string> index);
        int ModifyTableIndex(string tableName, KeyValuePair<string, string> index);
        int DeleteTableIndex(string tableName, KeyValuePair<string, string> index);
        //查询表数据
        List<Dictionary<string, string>> GetTableData(string tableName);
        List<Dictionary<string, string>> GetTableDataByKeys(string tableName, string keyName, List<string> keys);
        List<Dictionary<string, string>> GetTableDataByPage(string tableName, int pageindex,int pagesize,string pkey ,string extraField= "pkIndex", KeyValuePair<string, string> orderby= new KeyValuePair<string, string>());
        //新增 修改 删除
        int InsertData(string table, List<Dictionary<string, string>> datas);
        int DeleteData(string table, string pkey,List<string> ids);
        int UpdateData(string table, List<Dictionary<string, string>> datas);
        //获取查询数据
        string getData(string sql,int index=0);
        List<List<string>> getDatas(string sql);

        List<Dictionary<string, string>> getDatasDic(string sql, List<string> columnSelect = null, int columnSelectType = 1);
        //执行sql语句
        int Execute(string sql);
    }
}
