﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbSync.Provider
{
    class DbProxy
    {
        private DbProxy()
        {

        }
        public static IDbProxy GetProviderFactory(string ConnectionString, string dbtype = "MySql")         
        {
            if (dbtype == "MySql") {
                return new MySqlProxy(ConnectionString);
            }
            if (dbtype == "MsSql")
            {
                return new MsSqlProxy(ConnectionString);
            }
            return new MySqlProxy(ConnectionString);
        }
        public static bool DicCompare(Dictionary<string, string> op1, Dictionary<string, string> op2)
        {
            foreach (var key in op1.Keys)
            {
                if (op2.ContainsKey(key))
                {
                    if (op1[key] != op2[key])
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            return true;
        }
    }
}
