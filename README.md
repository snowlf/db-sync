
# DbSync

> DbSync 是一款使用 .Net4.5(可以转Core) 作为基础框架开发的,目前运行在windows平台的数据库同步工具。此类工具开源社区有很多,这里不是为了重复造轮子,仅仅是因为公司业务需要,不建议直接在生产环境上使用。

## 项目介绍

DbSync 运行在windows平台的数据库同步工具
* 支持一主多从同步
* 支持同步方式设设置(结构,索引,增量,全量)
* 支持指定表同步和忽略表同步
* 支持同步计划,定时同步


## 项目计划
* MySQl
* *MSSQL*
* *Oracle*
* *Sqlite*
* *其他*
* *异构数据库同步*
* *GUI界面*

## 展示
![项目结构](https://images.gitee.com/uploads/images/2021/0304/165129_b63078e0_714548.png "项目.png")
![配置说明](https://images.gitee.com/uploads/images/2021/0304/165150_5334068a_714548.png "config.png")
![运行截图](https://images.gitee.com/uploads/images/2021/0304/165207_be4ae159_714548.png "run.png")
![错误日志](https://images.gitee.com/uploads/images/2021/0304/165236_6390c224_714548.png "log.png")




## 信息获取

本人QQ: 724926089,代码比较简单,有需要支持的地方可以Q我.

## 开发日志

* 2021-03-4: 更新基本使用说明。

